package graphic

import (
	"fmt"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

//MoveView moves what you are seeing on screen
func (window *Window) MoveView(x, y int32) {

	//dont move out of the background
	if window.background.SRect.X+x < 0 {
		x = 0 - window.background.SRect.X
	}
	if window.background.SRect.Y+y < 0 {
		y = 0 - window.background.SRect.Y
	}
	if window.background.SRect.X+x+window.background.SRect.W > window.background.W {
		x = window.background.W - (window.background.SRect.X + window.background.SRect.W)
	}
	if window.background.SRect.Y+y+window.background.SRect.H > window.background.H {
		y = window.background.H - (window.background.SRect.Y + window.background.SRect.H)
	}

	window.background.SRect.X += x
	window.background.SRect.Y += y

	for _, i := range window.Sprites {
		for j := i.instances.Front(); j != nil; j = j.Next() {
			if instance, ok := j.Value.(*Instance); ok {
				instance.DestRect.X += float32(x)
				instance.DestRect.Y += float32(y)
			} else {
				fmt.Println("list of sprite does not contain Instances")
			}
		}
	}
}

//SetView sets the x and y value of the viewport
func (window *Window) SetView(x, y int32) {
	window.MoveView(x-window.background.SRect.X, y-window.background.SRect.Y)
}

//LoadBackgroundImg loads an image and uses it as the background
func (window *Window) LoadBackgroundImg(imgPath string) error {
	windowW, windowH, err := window.Renderer.GetOutputSize()
	if err != nil {
		window.logger.Println(err)
		return err
	}
	window.background.Texture, err = img.LoadTexture(window.Renderer, imgPath)
	if err != nil {
		window.logger.Println(err)
		return err
	}

	_, _, window.background.W, window.background.H, err = window.background.Texture.Query()
	if err != nil {
		window.logger.Println(err)
		return err
	}

	window.background.DRect.X = 0
	window.background.DRect.Y = 0
	window.background.DRect.W = windowW
	window.background.DRect.H = windowH
	window.background.SRect.X = 0
	window.background.SRect.Y = 0
	window.background.SRect.W = windowW
	window.background.SRect.H = windowH

	return nil
}

//RenderBackground by rendering current instances to work instances have to be created and should probably be dumped along with the sprites
func (window *Window) RenderBackground(w, h int32) error {
	var err error

	window.Renderer.SetDrawColor(window.DrawColor.r, window.DrawColor.g, window.DrawColor.b, window.DrawColor.a)
	window.background.Texture, err = window.Renderer.CreateTexture(uint32(sdl.PIXELFORMAT_RGBA32), sdl.TEXTUREACCESS_TARGET, w, h)

	if err != nil {
		window.logger.Println(err)
		return err
	}

	err = window.Renderer.SetRenderTarget(window.background.Texture)

	if err != nil {
		window.logger.Println(err)
		return err
	}

	err = window.Renderer.Clear()

	if err != nil {
		window.logger.Println(err)
		return err
	}

	for _, i := range window.Sprites {
		for j := i.instances.Front(); j != nil; j = j.Next() {
			if instance, ok := j.Value.(*Instance); ok {
				err = window.Renderer.CopyExF(i.texture, &i.srcRect, &instance.DestRect, instance.Angle, &instance.Center, sdl.FLIP_HORIZONTAL)
				if err != nil {
					window.logger.Println(err)
					return err
				}
			} else {
				fmt.Println("list of sprite does not contain Instances")
			}
		}
	}
	window.Renderer.Present()

	return nil
}
