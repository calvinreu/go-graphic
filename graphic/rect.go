package graphic

import "github.com/veandco/go-sdl2/sdl"

func NewFRect(x, y, w, h float32) sdl.FRect {
	var retVal sdl.FRect

	retVal.X = x
	retVal.Y = y
	retVal.W = w
	retVal.H = h
	return retVal
}

func NewRect(x, y, w, h int32) sdl.Rect {
	var retVal sdl.Rect

	retVal.X = x
	retVal.Y = y
	retVal.W = w
	retVal.H = h
	return retVal
}
