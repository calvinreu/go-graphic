package graphic

import (
	"testing"
)

var windowConfig WindowConfig

func EmptySpriteConf() []SpriteBaseConfig {
	retVal := make([]SpriteBaseConfig, 2)

	retVal[0].Sprites = make([]SpriteConfig, 2)
	retVal[1].Sprites = make([]SpriteConfig, 2)
	return retVal
}

func TestIsInit(t *testing.T) {
	var window Window
	if window.IsInit() {
		t.Errorf("window wasn't initialized")
	}
	err := window.Init(windowConfig)
	if !window.IsInit() && err == nil {
		t.Errorf("window was initialized")
	}
}

func TestAddSprite(t *testing.T) {
	var window Window
	window.Init(windowConfig)
	ID, err := window.AddSprite("test/img/tile.png", NewRect(0, 0, 0, 0))
	if err != nil && ID >= 0 {
		t.Errorf("ID does not match error return")
	}

	if len(window.Sprites)-1 != ID {
		t.Errorf("ID is incorrect")
	}
}
func TestAddSpriteByID(t *testing.T) {
	var window Window
	window.Init(windowConfig)
	ID := window.AddSpriteByID(-1, NewRect(0, 0, 0, 0))
	if ID != -1 {
		t.Errorf("ID does not indicate error")
	}
	ID, _ = window.AddSprite("test/img/tile.png", NewRect(0, 0, 0, 0))
	if ID != -1 {
		window.AddSpriteByID(ID, NewRect(0, 0, 0, 0))
	}
}
func TestRender(t *testing.T) {
	var window Window
	window.Init(windowConfig)

	window.Render()
}

func TestInit(t *testing.T) {
	var window Window
	var config Config
	config.Load("test/config/window.json", "test/config/sprite.json")
	window.Init(config.Window)
}

func TestLoadSprites(t *testing.T) {
	var window Window
	window.Init(windowConfig)

	window.LoadSprites(EmptySpriteConf())
}

func TestLoadAndDumpSprites(t *testing.T) {
	{
		var window Window
		window.Init(windowConfig)

		window.LoadAndDumpSprites(EmptySpriteConf())
	}
	{
		var window Window
		window.Init(windowConfig)
		window.LoadSprites(EmptySpriteConf())
		window.LoadAndDumpSprites(EmptySpriteConf())
	}
}
