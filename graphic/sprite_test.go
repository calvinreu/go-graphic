package graphic

import (
	"testing"
)

func TestNewSprite(t *testing.T) {
	var window Window
	window.Init(windowConfig)

	NewSprite(window.Renderer, "test/img/tile.png", NewRect(0, 0, 0, 0))
}

func TestNewInstance(t *testing.T) {
	var window Window
	window.Init(windowConfig)

	sprite, _ := NewSprite(window.Renderer, "test/img/tile.png", NewRect(0, 0, 0, 0))
	sprite.NewInstance(0, 0)
}
