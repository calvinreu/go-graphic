package main

import (
	"fmt"
	"time"

	"gitlab.com/calvinreu/graphic/graphic"
)

func main() {
	var config graphic.Config
	var window graphic.Window
	config.Load("config/window.json", "config/sprite.json")
	window.Init(config.Window)
	spriteIDs, _ := window.LoadSprites(config.BaseSprites)
	creature, water, gras, desert := spriteIDs["creature"], spriteIDs["water"], spriteIDs["gras"], spriteIDs["desert"]
	fmt.Println(spriteIDs)

	window.Sprites[water].NewInstance(25, 25)
	window.Sprites[water].NewInstance(125, 75)
	window.Sprites[gras].NewInstance(75, 25)
	window.Sprites[gras].NewInstance(75, 75)
	window.Sprites[desert].NewInstance(125, 25)
	window.Sprites[desert].NewInstance(25, 75)
	pacman := window.Sprites[creature].NewInstance(100, 100)
	fmt.Println("starting render")
	for i := 0; i < 200; i++ {
		err := window.Render()
		if err != nil {
			fmt.Println(err)
			return
		}
		pacman.ChangePosition(-0.5, -0.5)
		pacman.Angle += 1
		time.Sleep(20 * time.Millisecond)
	}
	fmt.Println("end")
	window.Quit()
}
