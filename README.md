# go-graphic

[![pipeline status](https://gitlab.com/calvinreu/graphic/badges/main/pipeline.svg)](https://gitlab.com/calvinreu/graphic/-/commits/main)

## dependendencies
to build [go-sdl](https://github.com/veandco/go-sdl2) is required

## info
go graphic is a library to quickly create a simple graphical output.
NOTE: this project is currently may not work or frequently break with updates

## version
this is the second stable version 0.2
Features:
 - load sprites from a json config + img files
 - create window from config file
 - log informantion in /tmp with the names WindowConfig.log SpriteConfig.log and *name of the window*.log
 - Render function to render every instance
 - add instances to every sprite
 - control instances by only changing der position tilt and center
 - add Sprite manually with imgPath or by ID of another sprite
 - render background with the same tools to render scenes
 - load background from image file
 - dump sprites
 - move the viewport around